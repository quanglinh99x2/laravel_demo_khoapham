<?php

use Illuminate\Support\Facades\Route;
use App\TheLoai;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::get('admin/login','UserController@get_login_admin');
Route::get('admin/logout','UserController@logout_admin');
Route::post('admin/login','UserController@post_login_admin');
//=================ROUTE ADMIN==================//

Route::group(['prefix' => 'admin','middleware'=>'adminLogin'], function () {
    //Thể loại
    Route::group(['prefix' => 'theloai'], function () {
        Route::get('list', 'TheLoaiController@get_view_list');

        Route::get('edit/{id}', 'TheLoaiController@get_view_edit');
        Route::post('edit/{id}','TheLoaiController@post_edit');

        Route::get('add', 'TheLoaiController@get_view_add');
        Route::post('add','TheLoaiController@post_add');

        Route::get('delete/{id}','TheLoaiController@delete');
    });
    //Loại tin
    Route::group(['prefix' => 'loaitin'], function () {
        Route::get('list', 'LoaiTinController@get_view_list');

        Route::get('edit/{id}', 'LoaiTinController@get_view_edit');
        Route::post('edit/{id}','LoaiTinController@post_edit');

        Route::get('add', 'LoaiTinController@get_view_add');
        Route::post('add','LoaiTinController@post_add');

        Route::get('delete/{id}','LoaiTinController@delete');
    });
    //Tin tức
    Route::group(['prefix' => 'tintuc'], function () {
        Route::get('list', 'TinTucController@get_view_list');

        Route::get('edit/{id}', 'TinTucController@get_view_edit');
        Route::post('edit/{id}','TinTucController@post_edit');

        Route::get('add', 'TinTucController@get_view_add');
        Route::post('add', 'TinTucController@post_add');


        Route::get('delete/{id}','TinTucController@delete');
    });
    //user
    Route::group(['prefix' => 'users'], function () {
        Route::get('list', 'UserController@get_view_list');

        Route::get('edit/{id}', 'UserController@get_view_edit');
        Route::post('edit/{id}', 'UserController@post_edit');

        Route::get('add', 'UserController@get_view_add');
        Route::post('add','UserController@post_add');

        Route::get('delete/{id}','UserController@delete');
    });
    //slide
    Route::group(['prefix' => 'slide'], function () {
        Route::get('list', 'SlideController@get_view_list');

        Route::get('edit/{id}', 'SlideController@get_view_edit');
        Route::post('edit/{id}', 'SlideController@post_edit');
        Route::get('add', 'SlideController@get_view_add');
        Route::post('add','SlideController@post_add');

        Route::get('delete/{id}','SlideController@delete');

    });
    Route::group(['prefix' => 'ajax'], function () {
        Route::get('loaitin/{idTheLoai}','AjaxController@getLoaiTin');
    });
    Route::group(['prefix' => 'comment'], function () {
        Route::get('delete/{idComment}','CommentController@delete');
    });

});
Route::get('home','PagesController@home');
Route::get('contact','PagesController@contact');
Route::get('loaitin/{id}/{TenKhongDau}.html','PagesController@loaitin');
Route::get('tintuc/{id}/{TieuDeKhongDau}.html','PagesController@tintuc');

//login-logout
Route::get('login','PagesController@getLogin');
Route::post('login','PagesController@postLogin');
Route::get('logout','PagesController@getLogout');

//comment
Route::post('comment/{id}','CommentController@postComment');
//user management
Route::get('user','PagesController@getUser');
Route::post('user/{id}','PagesController@postUser');

//register
Route::get('registerUser','PagesController@getRegisterUser');
Route::post('registerUser','PagesController@postRegisterUser');