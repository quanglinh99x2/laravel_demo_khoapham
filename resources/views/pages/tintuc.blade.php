@extends('layout.index')
@section('content')
<div class="container">
    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-9">

            <!-- Blog Post -->

            <!-- Title -->
            <h1>{{ $tintuc->TieuDe }}</h1>

            <!-- Author -->
            <p class="lead">
                by <a href="#">Admin</a>
            </p>

            <!-- Preview Image -->
            <img class="img-responsive" style="width:600px; height:300px"src="upload/tintuc/{{ $tintuc->Hinh }}" alt="">
            <br>
            <!-- Date/Time -->
            <p><span class="glyphicon glyphicon-time"></span> Posted on :{{ $tintuc->created_at }}</p>
            <hr>

            <!-- Post Content -->
            <p class="lead">{!! $tintuc->NoiDung !!}</p>
            </p>

            <hr>
            <!-- Blog Comments -->

            <!-- Comments Form -->
            @if(Auth::check())
                <div class="well">
                    @if (session('thongbao'))
                          <div class="alert alert-success">
                              {{ session('thongbao') }}
                          </div>
                          @endif
                    <h4>Viết bình luận ...<span class="glyphicon glyphicon-pencil"></span></h4>
                    <form role="form" action="comment/{{ $tintuc->id }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <textarea class="form-control" name="NoiDung" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Gửi</button>
                    </form>
                </div>
            @endif

            <hr>
            <!-- Posted Comments -->
            @foreach ($tintuc->comment as $item)
                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{ $item->user->name }}
                            <small>{{ $item->created_at }}</small>
                        </h4>
                        {{ $item->NoiDung }}
                    </div>
                </div>

                <!-- Comment -->
            @endforeach



        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-3">

            <div class="panel panel-default">
                <div class="panel-heading"><b>Tin liên quan</b></div>
                <div class="panel-body">
                    @foreach ($tin_lien_quan as $item)
                            <!-- item -->
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-5">
                                <a href="tintuc/{{ $item->id }}/{{ $item->TieuDeKhongDau }}.html">
                                    <img class="img-responsive"style="" src="upload/tintuc/{{ $item->Hinh }}" alt="">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <a href="tintuc/{{ $item->id }}/{{ $item->TieuDeKhongDau }}.html"><b>{{ $item->TieuDe }}</b></a>
                            </div>
                            <p></p>
                            <div class="break"></div>
                        </div>
                        <!-- end item -->
                    @endforeach


                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><b>Tin nổi bật</b></div>
                <div class="panel-body">
                    @foreach($tin_noi_bat as $item)
                    <!-- item -->
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-5">
                            <a href="tintuc/{{ $item->id }}/{{ $item->TieuDeKhongDau }}.html">
                                <img class="img-responsive" src="upload/tintuc/{{ $item->Hinh }}" alt="">
                            </a>
                        </div>
                        <div class="col-md-7">
                            <a href="tintuc/{{ $item->id }}/{{ $item->TieuDeKhongDau }}.html"><b>{{ $item->TieuDe }}</b></a>
                        </div>
                        <p></p>
                        <div class="break"></div>
                    </div>
                    <!-- end item -->
                    @endforeach
                </div>
            </div>

        </div>

    </div>
    <!-- /.row -->
</div>
@endsection
