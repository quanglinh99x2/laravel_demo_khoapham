@extends('layout.index')

@section('content')
<div class="container">

    <!-- slider -->
    <div class="row carousel-holder">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                  <div class="panel-heading">Thông tin tài khoản</div>
                  <div class="panel-body">
                    @if(Auth::check())
                    @if (count($errors)>0)
                          <div class="alert alert-danger">
                              @foreach ($errors->all() as $item)
                                  {{ $item }} <br>
                              @endforeach
                          </div>
                          @endif
                          @if (session('thongbao'))
                          <div class="alert alert-success">
                              {{ session('thongbao') }}
                          </div>
                          @endif
                        <form method="POST" action="user/{{Auth::user()->id}}">
                      @csrf
                        <div>
                            <label>Họ tên</label>
                        <input type="text" class="form-control" placeholder="Username" value="{{Auth::user()->name}}" name="name" aria-describedby="basic-addon1">
                        </div>
                        <br>
                        <div>
                            <label>Email</label>
                              <input type="email" class="form-control" placeholder="Email" value="{{Auth::user()->email}}" name="email" aria-describedby="basic-addon1"
                              disabled
                              >
                        </div>
                        <br>	
                        <div>
                            <input type="checkbox" id="check-repass" name="check_repass">
                            <label>Đổi mật khẩu</label>
                              <input type="password" class="form-control pass" disabled name="password" aria-describedby="basic-addon1">
                        </div>
                        <br>
                        <div>
                            <label>Nhập lại mật khẩu</label>
                              <input type="password" class="form-control pass" disabled name="passwordAgain" aria-describedby="basic-addon1">
                        </div>
                        <br>
                        <button type="submit" class="btn btn-default" value="">Sửa
                        </button>
                    </form>
                    @endif
                  </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
    <!-- end slide -->
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#check-repass').change(function(){
                if($(this).is(':checked'))
                {
                    $(".pass").removeAttr('disabled');
                }else{
                    $(".pass").attr('disabled','');
                }
            });
        });
    </script>
@endsection