 {{-- thừa kế từ trang index --}}
 @extends('admin.layout.index')

 @section('content')
      <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tin tức
                    <small>Thêm</small>
                </h1>


            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
            @if (count($errors) >0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $err)
                        {{ $err }} <br>
                    @endforeach
                </div>
            @endif
            @if (session('thongbao'))
                <div class="alert alert-success">
                    {{ session('thongbao') }}
                </div>
            @endif

            @if (session('error_img'))
            <div class="alert alert-danger">
                {{ session('error_img') }}
            </div>
            @endif

                <form action="admin/tintuc/add" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Thể loại</label>
                        <select class="form-control" id="the-loai" name="TheLoai_id">
                            @foreach ($theloai as $tl)
                                 <option value="{{ $tl->id }}">{{ $tl->Ten }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Loại tin</label>
                        <select class="form-control" id="loai-tin"  name="LoaiTin_id">
                            @foreach ($loaitin as $lt)
                                 <option value="{{ $lt->id }}">{{ $lt->Ten }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input class="form-control" name="tieu_de" placeholder="Nhập tiêu đề" />
                    </div>
                    {{-- tóm tắt --}}
                    <div class="form-group">
                        <label>Tóm tắt</label>
                        <textarea id="demo" class="ckeditor" name="tom_tat" rows="3"></textarea>
                    </div>
                    {{-- nội dung --}}
                    <div class="form-group">
                        <label>Nội dung</label>
                        <textarea id="demo" class="ckeditor" name="noi_dung" rows="3"></textarea>
                    </div>
                    {{-- up hình  --}}
                    <div class="form-group">
                        <label>Hình ảnh</label>
                        <input type="file" name="Hinh" id="" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Nổi bật</label>
                        <label class="radio-inline">
                            <input name="noi_bat" value="0" checked="" type="radio">Không nổi bật
                        </label>
                        <label class="radio-inline">
                            <input name="noi_bat" value="1" type="radio">Nổi bật
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Thêm</button>
                    <button type="reset" class="btn btn-default">Làm mới</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
 @endsection
{{-- AJAX lấy thông tin loại tin theo thể loại --}}
@section('script')
    <script>
        $(document).ready(function () {
            $('#the-loai').change(function (e) {
                e.preventDefault();
                var idTheLoai = $(this).val();
                $.get("admin/ajax/loaitin/"+idTheLoai,function(data){
                    $("#loai-tin").html(data);
                });
            });
        });
    </script>
@endsection
