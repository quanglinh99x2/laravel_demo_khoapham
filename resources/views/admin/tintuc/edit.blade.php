 {{-- thừa kế từ trang index --}}
 @extends('admin.layout.index')

 @section('content')
      <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tin tức
                    <small>{{ $tintuc->TieuDe }}</small>
                </h1>


            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if (count($errors) >0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $err)
                        {{ $err }} <br>
                    @endforeach
                </div>
            @endif
            @if (session('thongbao'))
                <div class="alert alert-success">
                    {{ session('thongbao') }}
                </div>
            @endif

            @if (session('error_img'))
            <div class="alert alert-danger">
                {{ session('error_img') }}
            </div>
            @endif

              
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên người dùng</th>
                                <th>Tin tức</th>
                                <th>Nội dung</th>
                                <th>Thời gian</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (isset($tintuc->comment))
                                @foreach ($tintuc->comment as $cm)
                                <tr class="odd gradeX" align="center">
                                    <td>{{ $cm->id }}</td>
                                    <td><p>{{ $cm->user->name }}</p>
                                    </td>
                                    <td>{{ $cm->tintuc->TieuDe }}</td>
                                    <td>{{ $cm->NoiDung}}</td>
                                    <td>{{ $cm->create_at }}</td>
                                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/comment/delete/{{ $cm->id }}"> Delete</a></td>
                                </tr>
                                @endforeach
                            @endif


                        </tbody>
                    </table>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
 @endsection
{{-- AJAX lấy thông tin loại tin theo thể loại --}}
@section('script')
    <script>
        $(document).ready(function () {
            $('#the-loai').change(function (e) {
                e.preventDefault();
                var idTheLoai = $(this).val();
                $.get("admin/ajax/loaitin/"+idTheLoai,function(data){
                    $("#loai-tin").html(data);
                });
            });
        });
    </script>
@endsection
