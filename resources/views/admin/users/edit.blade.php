@extends('admin.layout.index')

@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User : {{ $user->name }}
                        <small>Edit</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    @if (count($errors) >0)
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $err)
                                {{ $err }} <br>
                            @endforeach
                        </div>
                    @endif
                     @if (session('thongbao'))
                        <div class="alert alert-success">
                            {{ session('thongbao') }}
                        </div>
                     @endif
                    <form action="admin/users/edit/{{ $user->id }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Họ tên</label>
                            <input type="text" class="form-control" value="{{ $user->name }}" name="name" placeholder="Nhập tên người dùng" />
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" readonly="true" value="{{ $user->email }}" name="email" placeholder="Nhập email" />
                        </div>
                        <div class="form-group">
                           <input type="checkbox" name="check_repass" id="check-repass"> <label>Đổi mật khẩu</label>
                            <input type="password" disabled="" value="" class="form-control pass" name="password" placeholder="Nhập mật khẩu" />
                        </div>
                        <div class="form-group">
                            <label>Nhập lại Password</label>
                            <input type="password" disabled="" class="form-control pass" name="password_again" placeholder="Nhập mật khẩu" />
                        </div>
                        <div class="form-group">
                            <label>Quyền người dùng</label>
                            <label class="radio-inline">
                                <input name="rule" value="0"
                                @if ($user->quyen ==0)
                                    {{ "checked" }}
                                @endif
                                type="radio">Thường
                            </label>
                            <label class="radio-inline">
                                <input name="rule"
                                @if ($user->quyen ==1)
                                    {{ "checked" }}
                                @endif
                                value="1" type="radio">Admin
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default">Thêm</button>
                        <button type="reset" class="btn btn-default">Làm mới</button>

                    <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#check-repass').change(function(){
                if($(this).is(':checked'))
                {
                    $(".pass").removeAttr('disabled');
                }else{
                    $(".pass").attr('disabled','');
                }
            });
        });
    </script>
@endsection
