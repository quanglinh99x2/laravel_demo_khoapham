@extends('admin.layout.index')

@section('content')
     <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Users
                    <small>List</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        @if (session('thongbao'))
            <div class="alert alert-success">
                {{ session('thongbao') }}
            </div>
        @endif
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr align="center">
                            <th>ID</th>
                            <th>Name</th>
                            <th>email</th>
                            <th>Quyền</th>
                            <th>Ngày lập</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                        <tbody>
                            @if (isset($user))
                                @foreach ($user as $item)
                                <tr class="odd gradeX" align="center">
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->quyen }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/users/delete/{{ $item->id }}"> Xóa</a></td>
                                    <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/users/edit/{{ $item->id }}">Sửa</a></td>
                                </tr>
                                @endforeach
                                @endif
                        </tbody>
                </table>
            </table>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

@endsection
