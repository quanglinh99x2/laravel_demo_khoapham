@extends('admin.layout.index')

@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Category
                        <small>Edit</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    @if (count($errors) >0)
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $err)
                            {{ $err }} <br>
                        @endforeach
                    </div>
                @endif
                @if (session('thongbao'))
                    <div class="alert alert-success">
                        {{ session('thongbao') }}
                    </div>
                @endif

                @if (session('error_img'))
                <div class="alert alert-danger">
                    {{ session('error_img') }}
                </div>
                @endif


                        <form action="admin/slide/edit/{{ $slide->id }}" method="POST" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label>Tên</label>
                                <input class="form-control" name="ten" value="{{ $slide->Ten }}" placeholder="Nhập tên slide" />
                            </div>

                            {{-- up hình  --}}
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <input type="file" name="Hinh" id="" class="form-control">
                               <p><img width="500px" src="upload/slide/{{ $slide->Hinh }}" alt="hình ảnh"></p>
                            </div>
                            {{-- nội dung --}}
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea id="demo" class="ckeditor" name="noi_dung" rows="3">{{ $slide->NoiDung }}</textarea>
                            </div>
                              {{-- link --}}
                              <div class="form-group">
                                <label>Link</label>
                                <input class="form-control" name="link" value="{{ $slide->link }}" placeholder="Nhập link" />
                            </div>

                            <button type="submit" class="btn btn-default">sửa</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>



                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@endsection
