<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoaiTin extends Model
{
    //
    protected $table = "loaitin";
    // 1-1 một loại tin thuộc 1 thể loại
    public function theloai(){
        return $this->belongsTo('App\TheLoai','idTheLoai','id');
    }
    //1-n một loại tin gồm nhiều tin
    public function tintuc(){
        return $this->hasMany('App\TinTuc','idTheLoai','id');
    }
}
