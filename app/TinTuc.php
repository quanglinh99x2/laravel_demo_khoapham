<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TinTuc extends Model
{
    //
    protected $table = "tintuc";
    //1-1 một tin tức thuộc 1 loại tin
    public function loaitin(){
        return $this->belongsTo('App\LoaiTin','idLoaiTin','id');
    }
    // 1-n một tin tức có nhiều comment
    public function comment(){
        return $this->hasMany('App\Comment','idTinTuc','id');
    }
}
