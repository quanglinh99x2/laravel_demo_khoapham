<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table = "comment";
    // 1-1 một commnet thuộc 1 tin tức
    public function tintuc(){
        return $this->belongsTo('App\TinTuc','idTinTuc','id');
    }
    //1-1 một comment thuộc 1 user
    public function user(){
        return $this->belongsTo('App\User','idUser','id');
    }
}
