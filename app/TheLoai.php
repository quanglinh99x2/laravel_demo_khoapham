<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TheLoai extends Model
{
    //
    protected $table = "theloai";
    public function loaitin(){
        return $this->hasMany('App\LoaiTin','idTheLoai','id');
    }
    //Liên kết tới bảng khác dùng hasManyThrough
    //Dùng để kiểm tra xem thể loại này thì có tất cả những tin tức gì...
    public function tintuc(){
        return $this->hasManyThrough('App\TinTuc','App\LoaiTin','idTheLoai','idLoaiTin','id');
    }
}
