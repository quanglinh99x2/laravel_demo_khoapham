<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
    //
    public function get_view_list(){
        $user = User::all();
        return view('admin.users.list',['user'=>$user]);
    }
    public function get_view_add(){
        return view('admin.users.add');
    }
    public function post_add(Request $request){
        $this->validate($request,
        [
            'name'=> 'required|min:3',
            'email'=> 'required|email|unique:users,email',
            'password' => 'required|min:6|max:32',
            'password_again'=>'required|same:password'
        ],
        [
            'name.required'=>'Bạn chưa nhập tên người dùng',
            'email.email'=>"định dạng email không hợp lệ",
            'name.min' => 'tên tối thiểu 3 ký tự',
            'email.required'=>'không được để trống email',
            'email.unique'=>'email đã tồn tại',
            'password.requied'=>'không được để trống password',
            'password.min'=>'độ dài mật khẩu tối thiểu từ 6 đến 32 ký tự',
            'password.max'=>'độ dài mật khẩu tối thiểu từ 6 đến 32 ký tự',
            'password_again'=> 'hãy nhập lại mật khẩu',
            'password_again.same'=>'mật khẩu nhập lại không khớp, kiểm tra lại'
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->quyen = $request->rule;
        $user->save();
        return redirect('admin/users/add')->with('thongbao','Thêm mới thành công!');
    }
    public function get_view_edit($id){
        $user = User::find($id);
        return view('admin.users.edit',['user'=>$user]);
    }
    public function post_edit(Request $request ,$id){
        $this->validate($request,
        [
            'name'=> 'required|min:3',
        ],
        [
            'name.required'=>'Bạn chưa nhập tên người dùng',
            'name.min' => 'tên tối thiểu 3 ký tự',
        ]);
        $user = User::find($id);
        $user->name = $request->name;
        if($request->check_repass == "on"){
            $this->validate($request,
        [
            'password' => 'required|min:6|max:32',
            'password_again'=>'required|same:password'
        ],
        [
            'password.requied'=>'không được để trống password',
            'password.min'=>'độ dài mật khẩu tối thiểu từ 6 đến 32 ký tự',
            'password.max'=>'độ dài mật khẩu tối thiểu từ 6 đến 32 ký tự',
            'password_again'=> 'hãy nhập lại mật khẩu',
            'password_again.same'=>'mật khẩu nhập lại không khớp, kiểm tra lại'
        ]);
        $user->password = bcrypt($request->password);
        }
        $user->quyen = $request->rule;
        $user->save();
        return redirect("admin/users/edit/".$id)->with("thongbao","Sửa thành công");
    }

    //Đăng nhập
    public function get_login_admin(){
        return view('admin.login');
    }
    public function delete($id){
        $user = User::find($id);

         $user->delete();
         return redirect('admin/users/list')->with('thongbao',"Xóa thành công user :$user->name");
    }
    public function post_login_admin(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|min:6|max:32'
        ],
        [
            'email.required'=>'Bạn chưa nhập email',
            'email.email'=>"định dạng email không hợp lệ",
            'password'=>"Bạn chưa nhập password",
            'password.min'=>"Password phải từ 6 đến 32 ký tự",
            'password.max'=>"Password phải từ 6 đến 32 ký tự",
        ]);

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return redirect('admin/theloai/list');
        }
        else{
            return redirect('admin/login')->with('thongbao','Tài khoản hoặc mật khẩu không chính xác!');
        }
    }
    public function logout_admin(){
        Auth::logout();
        return redirect('admin/login');
    }
}
