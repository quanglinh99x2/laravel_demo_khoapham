<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use Illuminate\Support\Str;

class SlideController extends Controller
{
    //
    public function get_view_list(){
        $slide = Slide::all();
        return view('admin.slide.list',['slide'=>$slide]);
    }
    public function get_view_add(){
        return view('admin.slide.add');
    }
    public function post_add(Request $request){

        $this->validate($request,
        [
            'ten'=> 'required|min:3|max:100',
            'link'=>'required',
            'noi_dung'=>'required'
        ],
        [
            'ten.required'=>"Bạn chưa nhập tên slide",
            'ten.min'=>"Tên phải từ 3 đến 100 ký tự",
            'ten.max'=>"Tên phải từ 3 đến 100 ký tự",
            'link.required'=> "bạn chưa nhập link",
            'noi_dung.required'=> "Bạn hãy nhập nội dung slide"
        ]);
        $slide = new Slide();
        $slide->Ten = $request->ten;
        $slide->NoiDung = $request->noi_dung;
        $slide->link= $request->link;
        if($request->hasFile('Hinh')){
            $file=$request->file('Hinh');

            $typeImg= $file->getClientOriginalExtension('Hinh');
            $type_allow = array('png','jpg','gif','jpeg');
                    //lấy tên hình
            $nameImg = $file->getClientOriginalName();
            if(!in_array($typeImg,$type_allow)){
                return redirect('admin/slide/add')->with('error_img','Đuôi file hình ảnh phải là png,jpg,gif,jped');
            }
            //đặt tên mới tránh tồn tại
            $nameImgAfter = Str::random(4)."_".$nameImg;
            //kiểm tra nếu trùng nó sẽ while lấy tên mới
            while(file_exists("upload/slide/".$nameImgAfter)){
                $nameImgAfter = Str::random(4)."_".$nameImg;
            }
            //Lưu hình vào tệp
            $file->move("upload/slide",$nameImgAfter);
            //lưu tên hình vào database
            $slide->Hinh=$nameImgAfter;

        }else{

            $slide->Hinh="";
        }
        $slide->save();
        return redirect('admin/slide/add')->with('thongbao','Đã thêm thành công!');
    }
    public function get_view_edit($id){
        $slide = Slide::find($id);
        return view('admin.slide.edit',['slide'=>$slide]);
    }
    public function post_edit(Request $request,$id){
        $this->validate($request,
        [
            'ten'=> 'required|min:3|max:100',
            'link'=>'required',
            'noi_dung'=>'required'
        ],
        [
            'ten.required'=>"Bạn chưa nhập tên slide",
            'ten.min'=>"Tên phải từ 3 đến 100 ký tự",
            'ten.max'=>"Tên phải từ 3 đến 100 ký tự",
            'link.required'=> "bạn chưa nhập link",
            'noi_dung.required'=> "Bạn hãy nhập nội dung slide"
        ]);
        $slide = Slide::find($id);
        $slide->Ten = $request->ten;
        $slide->NoiDung = $request->noi_dung;
        $slide->link= $request->link;
        if($request->hasFile('Hinh')){
            $file=$request->file('Hinh');

            $typeImg= $file->getClientOriginalExtension('Hinh');
            $type_allow = array('png','jpg','gif','jpeg');
                    //lấy tên hình
            $nameImg = $file->getClientOriginalName();
            if(!in_array($typeImg,$type_allow)){
                return redirect('admin/slide/add')->with('error_img','Đuôi file hình ảnh phải là png,jpg,gif,jped');
            }
            //đặt tên mới tránh tồn tại
            $nameImgAfter = Str::random(4)."_".$nameImg;
            //kiểm tra nếu trùng nó sẽ while lấy tên mới
            while(file_exists("upload/slide/".$nameImgAfter)){
                $nameImgAfter = Str::random(4)."_".$nameImg;
            }
            //Lưu hình vào tệp
            $file->move("upload/slide",$nameImgAfter);
            //lưu tên hình vào database
            $slide->Hinh=$nameImgAfter;

        }
        $slide->save();
       return redirect('admin/slide/edit/'.$id)->with('thongbao','Sửa thành công');
    }
    public function delete($id){
        $slide = Slide::find($id);
        $slide->delete();
        return redirect('admin/slide/list')->with('thongbao','Đã xóa thành công!');
    }
}
