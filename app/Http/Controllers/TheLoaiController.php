<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
use  \Illuminate\Database\QueryException;
class TheLoaiController extends Controller
{
    //
    public function get_view_list(){
        $theloai=TheLoai::all();
        $list_theloai =TheLoai::paginate(5);
        return view('admin.theloai.list',['list_theloai'=>$list_theloai,'count_element'=>$theloai->count()]);

    }
    public function get_view_add(){
        return view('admin.theloai.add');
    }
    //nhận dữ liệu từ view về thông qua route
    public function post_add(Request $request){
        $this->validate($request,
        [
            'Ten'=>'required|unique:theloai,Ten|min:3|max:100'
        ],
        [
            'Ten.required'=>'Bạn chưa nhập tên thể loại',
            'Ten.unique'=>'Tên thể loại đã tồn tại',
            'Ten.min'=>'Số lượng ký tự phải từ 3 đến 100 ký tự',
            'Ten.max'=>'Số lượng ký tự phải từ 3 đến 100 ký tự'

        ]);
        $theloai = new TheLoai();
        $theloai->Ten= $request->Ten;
        $theloai->TenKhongDau= changeTitle($request->Ten);
        $theloai->save();

        return redirect('admin/theloai/add')->with('thongbao','Thêm thành công !');
    }
    public function get_view_edit($id){
        $theloai = TheLoai::find($id);
        return view('admin/theloai/edit',['theloai'=>$theloai]);
    }
    public function post_edit(Request $request,$id){
        $theloai= TheLoai::find($id);
        $this->validate($request,
        [
            'Ten'=>'required|unique:theloai,Ten|min:3|max:100'
        ],
        [
            'Ten.required'=>'Bạn chưa nhập thể loại',
            'Ten.unique'=>'Tên thể loại đã tồn tại',
            'Ten.min'=>'Độ dài tên phải từ 3 đến 100 ký tự',
            'Ten.max'=>'Độ dài tên phải từ 3 đến 100 ký tự'
        ]
        );
        $theloai->Ten= $request->Ten;
        $theloai->TenKhongDau= changeTitle($request->Ten);
        $theloai->save();

        return redirect('admin/theloai/edit/'.$id)->with('thongbao','Cập nhật thành công');
    }

    public function delete($id){
        $theloai=TheLoai::find($id);

        $theloai->delete();
        return redirect('admin/theloai/list')->with('thongbao',"Xóa thành công thể loại: $theloai->Ten");
    }
}
