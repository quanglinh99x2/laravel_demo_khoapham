<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\TinTuc;
use Illuminate\Support\Facades\Auth;
class CommentController extends Controller
{
    //
    public function delete($id){
        $comment = Comment::find($id);
        $comment->delete();
        return redirect('admin/tintuc/edit/'.$comment->tintuc->id)->with('thongbao','Đã xóa comment thành công');
    }
    function postComment(Request $request,$id){
        $idTintuc = $id;
        $tintuc = TinTuc::find($id);
        $comment = new Comment();
        $comment->idTinTuc =$idTintuc;
        $comment->idUser = Auth::user()->id;
        $comment->NoiDung = $request->NoiDung;
        $comment->save();

        return redirect("tintuc/$id/".$tintuc->TieuDeKhongDau.".html")->with('thongbao','Viết bình luận thành công!');
    }
}
