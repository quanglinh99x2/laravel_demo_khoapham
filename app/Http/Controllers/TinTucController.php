<?php

namespace App\Http\Controllers;

use App\TheLoai;
use Illuminate\Http\Request;
use App\TinTuc;
use App\LoaiTin;
use Illuminate\Support\Str;

class TinTucController extends Controller
{
    //
    public function get_view_list(){
        $tintuc = TinTuc::orderBy('id','DESC')->paginate(5);

        return view('admin.tintuc.list',['tintuc'=>$tintuc]);
    }
    public function get_view_add(){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        return view('admin.tintuc.add',['theloai'=>$theloai,'loaitin'=>$loaitin]);
    }
    public function post_add(Request $request){
        $this->validate($request,
        [
            'LoaiTin_id'=>'required',
            'tieu_de'=>'required|min:3|unique:tintuc,TieuDe',
            'tom_tat'=>'required',
            'noi_dung'=>'required'
        ],
        [
            'LoaiTin_id.required'=>'Bạn chưa chọn loại tin',
            'tieu_de.required'=> 'Bạn chưa nhập tiêu đề',
            'tieu_de.min'=>'Tiêu đề phải có ít nhất 3 ký tự',
            'tieude.unique'=>'Tiêu đề đã tồn tại',
            'tom_tat.required'=>'Bạn chưa nhập tóm tắt',
            'noi_dung.required'=>' Bạn chưa nhập nội dung'
        ]);
        $tintuc = new TinTuc();
        $tintuc->TieuDe = $request->tieu_de;
        $tintuc->TieuDeKhongDau = changeTitle($request->tieu_de);
        $tintuc->idLoaiTin = $request->LoaiTin_id;
        $tintuc->TomTat = $request->tom_tat;
        $tintuc->NoiDung = $request->noi_dung;
        $tintuc->SoLuotXem=0;
        $tintuc->NoiBat=$request->noi_bat;
        //Kiểm tra có truyền hình ảnh lên không
        if($request->hasFile('Hinh')){
            $file=$request->file('Hinh');
            //lấy tên hình
            $typeImg= $file->getClientOriginalExtension('Hinh');
            $type_allow = array('png','jpg','gif','jpeg');
            $nameImg = $file->getClientOriginalName();
            if(!in_array($typeImg,$type_allow)){
                return redirect('admin/tintuc/add')->with('error_img','Đuôi file hình ảnh phải là png,jpg,gif,jped');
            }
            //đặt tên mới tránh tồn tại
            $nameImgAfter = Str::random(4)."_".$nameImg;
            //kiểm tra nếu trùng nó sẽ while lấy tên mới
            while(file_exists("upload/tintuc/".$nameImgAfter)){
                $nameImgAfter = Str::random(4)."_".$nameImg;
            }
            //Lưu hình vào tệp
            $file->move("upload/tintuc",$nameImgAfter);
            //lưu tên hình vào database
            $tintuc->Hinh=$nameImgAfter;

        }else{

            $tintuc->Hinh="";
        }
        $tintuc->save();
        return redirect('admin/tintuc/add')->with('thongbao','Bạn đã thêm thành công một tin tức');
    }
    //sửa
    public function get_view_edit($id){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        $tintuc = TinTuc::find($id);
        return view('admin.tintuc.edit',['tintuc'=>$tintuc,'theloai'=>$theloai,'loaitin'=>$loaitin]);
    }
    public function post_edit(Request $request , $id){
        $tintuc = TinTuc::find($id);
        $this->validate($request,
        [
            'LoaiTin_id'=>'required',
            'tieu_de'=>'required|min:3',
            'tom_tat'=>'required',
            'noi_dung'=>'required'
        ],
        [
            'LoaiTin_id.required'=>'Bạn chưa chọn loại tin',
            'tieu_de.required'=> 'Bạn chưa nhập tiêu đề',
            'tieu_de.min'=>'Tiêu đề phải có ít nhất 3 ký tự',
            'tom_tat.required'=>'Bạn chưa nhập tóm tắt',
            'noi_dung.required'=>' Bạn chưa nhập nội dung'
        ]);
        $tintuc->TieuDe = $request->tieu_de;
        $tintuc->TieuDeKhongDau = changeTitle($request->tieu_de);
        $tintuc->idLoaiTin = $request->LoaiTin_id;
        $tintuc->TomTat = $request->tom_tat;
        $tintuc->NoiDung = $request->noi_dung;

        $tintuc->NoiBat=$request->noi_bat;
        if($request->hasFile('Hinh')){
            $file=$request->file('Hinh');
            //lấy tên hình
            $typeImg= $file->getClientOriginalExtension('Hinh');
            $type_allow = array('png','jpg','gif','jpeg');
            $nameImg = $file->getClientOriginalName();
            if(!in_array($typeImg,$type_allow)){
                return redirect('admin/tintuc/add')->with('error_img','Đuôi file hình ảnh phải là png,jpg,gif,jped');
            }
            //đặt tên mới tránh tồn tại
            $nameImgAfter = Str::random(4)."_".$nameImg;
            //kiểm tra nếu trùng nó sẽ while lấy tên mới
            while(file_exists("upload/tintuc/".$nameImgAfter)){
                $nameImgAfter = Str::random(4)."_".$nameImg;
            }
            //xóa file ảnh cũ
            unlink("upload/tintuc/".$tintuc->Hinh);
            //Lưu hình vào tệp
            $file->move("upload/tintuc",$nameImgAfter);
            //lưu tên hình vào database
            $tintuc->Hinh=$nameImgAfter;
        }
        $tintuc->save();
        return redirect("admin/tintuc/edit/".$id)->with('thongbao','sửa thành công');
    }
    public function delete($id){
        $tintuc = TinTuc::find($id);
        $tintuc->delete();
        return redirect('admin/tintuc/list')->with('thongbao','Xóa thành công');
    }

}
