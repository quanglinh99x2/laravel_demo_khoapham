<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
use App\LoaiTin;
class LoaiTinController extends Controller
{
    //
    public function get_view_list(){
        $loaitin=LoaiTin::all();
        $list_loaitin =LoaiTin::paginate(5);
        return view('admin.loaitin.list',['list_loaitin'=>$list_loaitin,'count_element'=>$loaitin->count()]);

    }
    public function get_view_add(){
        $theloai = TheLoai::all();
        return view('admin.loaitin.add',['theloai'=>$theloai]);
    }
    //nhận dữ liệu từ view về thông qua route
    public function post_add(Request $request){
        $this->validate($request,
        [
            'Ten'=>'required|unique:loaitin,Ten|min:2|max:100',
            'TheLoai_id' =>'required'
        ],
        [
            'Ten.required'=>'Bạn chưa nhập tên loại tin',
            'Ten.unique'=> 'Tên loại tin trùng',
            'Ten.min'=>'Tên loại tin phải có độ dài từ 2 đến 100 ký tự',
            'Ten.max'=>'Tên loại tin phải có độ dài từ 2 đến 100 ký tự',
            'TheLoai_id'=>'Bạn chưa chọn thể loại'
        ]);
        $loaitin = new LoaiTin();
        $loaitin->Ten = $request->Ten;
        $loaitin->TenKhongDau=changeTitle($request->Ten);
        $loaitin->idTheLoai=$request->TheLoai_id;
        $loaitin->save();

        return redirect('admin/loaitin/add')->with('thongbao','Bạn đã thêm thành công');
    }
    public function get_view_edit($id){
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::find($id);
        return view('admin/loaitin/edit',['loaitin'=>$loaitin ,'theloai'=>$theloai]);
    }
    public function post_edit(Request $request,$id){
        $this->validate($request,
        [
            'Ten'=>'required|min:2|max:100',
            'TheLoai_id' =>'required'
        ],
        [
            'Ten.required'=>'Bạn chưa nhập tên loại tin',

            'Ten.min'=>'Tên loại tin phải có độ dài từ 2 đến 100 ký tự',
            'Ten.max'=>'Tên loại tin phải có độ dài từ 2 đến 100 ký tự',
            'TheLoai_id'=>'Bạn chưa chọn thể loại'
        ]);
        $loaitin = LoaiTin::find($id);
        $loaitin->Ten = $request->Ten;
        $loaitin->TenKhongDau = changeTitle($loaitin->Ten);
        $loaitin->idTheLoai = $request->TheLoai_id;
        $loaitin->save();
        return redirect('admin/loaitin/edit/'.$id)->with('thongbao','Sửa thành công!');
    }
    public function delete($id){
        $loaitin = LoaiTin::find($id);
        $loaitin->delete();
        return redirect('admin/loaitin/list')->with('thongbao',"Bạn đã xóa thành công loại tin :$loaitin->Ten");;
    }
}
