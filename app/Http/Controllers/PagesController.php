<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
use App\Slide;
use App\LoaiTin;
use App\TinTuc;
use App\User;
use Illuminate\Support\Facades\Auth;
class PagesController extends Controller
{
    function __construct()
    {
        $theloai = TheLoai::all();
        $slide = Slide::all();
        view()->share('theloai',$theloai);
        view()->share('slide',$slide);
    }
    function home(){
        return view('pages.home');
    }
    function contact(){
        return view('pages.contact');
    }
    function loaitin($id){
        $loaitin = LoaiTin::find($id);
        $tintuc = TinTuc::where('idLoaiTin',$id)->paginate(5);
        return view('pages.loaitin',['loaitin'=>$loaitin,'tintuc'=>$tintuc]);
    }
    function tintuc($id){
        $tintuc=TinTuc::find($id);
        $tin_noi_bat =TinTuc::where('NoiBat',1)->take(5)->get();
        $tin_lien_quan = TinTuc::where('idLoaiTin',$tintuc->idLoaiTin)->take(5)->get();
        return view('pages.tintuc',['tintuc'=>$tintuc,'tin_noi_bat'=>$tin_noi_bat,'tin_lien_quan'=>$tin_lien_quan]);
    }
    function getLogin(){
        return view('pages.login');
    }
    function postLogin(Request $request){
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required|min:6|max:32'
        ],
        [
            'email.required'=>'Bạn chưa nhập email',
            'email.email'=>"định dạng email không hợp lệ",
            'password.required'=>"Bạn chưa nhập password",
            'password.min'=>"Password phải từ 6 đến 32 ký tự",
            'password.max'=>"Password phải từ 6 đến 32 ký tự",
        ]);
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return redirect('home');
        }
        else{
            return redirect('login')->with('thongbao','Tài khoản hoặc mật khẩu không chính xác!');
        }
    }
    function getLogout(){
        Auth::logout();
        return redirect('home');
    }
    
    //user
    function getUser(){
        return view('pages.user');
    }
    function postUser(Request $request,$id){
        $this->validate($request,
        [
            'name'=> 'required|min:3',
            'password'=>''
        ],
        [
            'name.required'=>'Bạn chưa nhập tên người dùng',
            'name.min' => 'tên tối thiểu 3 ký tự',
        ]);
        $user = User::find($id);
        $user->name = $request->name;
        if($request->check_repass == "on"){
            $this->validate($request,
            [
                'password' => 'required|min:6|max:32',
                'passwordAgain'=>'required|same:password'
            ],
            [
                'password.requied'=>'không được để trống password',
                'password.min'=>'độ dài mật khẩu tối thiểu từ 6 đến 32 ký tự',
                'password.max'=>'độ dài mật khẩu tối thiểu từ 6 đến 32 ký tự',
                'passwordAgain'=> 'hãy nhập lại mật khẩu',
                'passwordAgain.same'=>'mật khẩu nhập lại không khớp, kiểm tra lại'
            ]);
            $user->password = bcrypt($request->password);
        }
        $user->save();
        return redirect('user')->with('thongbao','Cập nhật thành công thông tin user!');

    }
    function getRegisterUser(){
        return view('pages.register');
    }
    function postRegisterUser(Request $request){
        $this->validate($request,
        [
            'name'=> 'required|min:3',
            'email'=> 'required|email|unique:users,email',
            'password' => 'required|min:6|max:32',
            'passwordAgain'=>'required|same:password'
        ],
        [
            'name.required'=>'Bạn chưa nhập tên người dùng',
            'email.email'=>"định dạng email không hợp lệ",
            'name.min' => 'tên tối thiểu 3 ký tự',
            'email.required'=>'không được để trống email',
            'email.unique'=>'email đã tồn tại',
            'password.requied'=>'không được để trống password',
            'password.min'=>'độ dài mật khẩu tối thiểu từ 6 đến 32 ký tự',
            'password.max'=>'độ dài mật khẩu tối thiểu từ 6 đến 32 ký tự',
            'passwordAgain'=> 'hãy nhập lại mật khẩu',
            'passwordAgain.same'=>'mật khẩu nhập lại không khớp, kiểm tra lại'
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->quyen = 0;
        $user->save();
        return redirect('registerUser')->with('thongbao','Thêm mới thành công!');
    }
}
